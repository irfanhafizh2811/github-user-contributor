package com.github.playground.di

import android.content.Context
import com.github.playground.R
import com.github.playground.app.AppExecutors
import com.github.playground.data.factory.LiveDataCallAdapterFactory
import com.github.playground.data.factory.MainDataSourceFactory
import com.github.playground.data.factory.RequestInterceptor
import com.github.playground.data.paging.MainPageDataSource
import com.github.playground.data.repository.MainRepository
import com.github.playground.data.service.MainService
import com.github.playground.di.NetworkModule.KEEP_ALIVE_DURATION
import com.github.playground.di.NetworkModule.MAX_IDLE_CONNECTIONS
import com.github.playground.di.NetworkModule.logLevel
import com.github.playground.extension.clazz
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val dataModule = module {
    single {
        HttpLoggingInterceptor().apply {
            level = logLevel(androidContext().getString(R.string.okhttp_log_level))
        }
    }
    single { makeConnectionPool() }
    single { makeCacheOkHttp(get()) }
    single { makeOkHttpClient(get(), get(), get(), get()) }
    single { RequestInterceptor() }

    single { GsonBuilder().create() }
    single {
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(get()))
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(get<OkHttpClient>())
            .baseUrl("https://api.github.com" + "/")
            .build()
    }
    single { get<Retrofit>().create(clazz<MainService>()) }
    single { AppExecutors() }
    single { MainRepository(get(), get()) }
    single { MainDataSourceFactory(get()) }
}


fun makeConnectionPool(): ConnectionPool {
    return ConnectionPool(MAX_IDLE_CONNECTIONS, KEEP_ALIVE_DURATION, TimeUnit.MILLISECONDS)
}

fun makeCacheOkHttp(context: Context): Cache {
    return Cache(
        context.externalCacheDir ?: context.cacheDir,
        NetworkModule.CACHE_DIR_SIZE_30MB.toLong()
    )
}

fun makeOkHttpClient(
    requestInterceptor: RequestInterceptor,
    httpLoggingInterceptor: HttpLoggingInterceptor,
    cache: Cache,
    connectionPool: ConnectionPool
): OkHttpClient {
    val timeout = NetworkModule.DEFAULT_TIMEOUT
    return OkHttpClient.Builder()
        .cache(cache)
        .readTimeout(timeout.toLong(), TimeUnit.SECONDS)
        .writeTimeout(timeout.toLong(), TimeUnit.SECONDS)
        .connectTimeout(timeout.toLong(), TimeUnit.SECONDS)
        .addInterceptor(requestInterceptor)
        .addInterceptor(httpLoggingInterceptor)
        .connectionPool(connectionPool)
        .build()
}

object NetworkModule {

    const val CACHE_DIR_SIZE_30MB = 30 * 1024 * 1024
    const val KEEP_ALIVE_DURATION = (30 * 1000).toLong()
    const val MAX_IDLE_CONNECTIONS = 10
    const val DEFAULT_TIMEOUT = 20

    fun logLevel(level: String?): HttpLoggingInterceptor.Level {
        return when (level) {
            "NONE" -> HttpLoggingInterceptor.Level.NONE
            "BASIC" -> HttpLoggingInterceptor.Level.BASIC
            "HEADERS" -> HttpLoggingInterceptor.Level.HEADERS
            "BODY" -> HttpLoggingInterceptor.Level.BODY
            else -> HttpLoggingInterceptor.Level.NONE
        }
    }

}