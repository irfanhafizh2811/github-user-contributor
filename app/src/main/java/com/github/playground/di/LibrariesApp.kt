package com.github.playground.di

val librariesModule = listOf(
    dataModule,
    viewModelModule
)