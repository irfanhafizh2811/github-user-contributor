package com.github.playground.data.response

data class ErrorResponse<T>(
    val errorCode: Int,
    val error: Throwable
) : DataResponse<T>()