package com.github.playground.data.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.github.playground.app.AppExecutors
import com.github.playground.data.response.ErrorResponse
import com.github.playground.data.response.Resource
import com.github.playground.data.response.SuccessResponse
import com.github.playground.data.response.ErrorCodeResponse
import com.github.playground.data.response.DataResponse
import java.io.IOException

abstract class RepositoryLiveData<Type>(
    private val appExecutors: AppExecutors,
    private val isLoadingPage: Boolean
) {

    init {
        setLoadingViewType()
        @Suppress("LeakingThis")
        val dbSource = loadFromLocal()
        result().addSource(dbSource) { data ->
            result().removeSource(dbSource)
            if (shouldFetchFromNetwork(data)) {
                fetchFromNetwork(dbSource)
            } else {
                result().addSource(dbSource) { newData ->
                    setValue(Resource.done(newData))
                }
            }
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<Type>) {
        if (result().value != newValue) {
            result().value = newValue
        }
    }

    private fun fetchFromNetwork(dbSource: LiveData<Type>) {
        val apiResponse = loadFromNetwork()
        // we re-attach dbSource as a new source, it will dispatch its latest value quickly
        result().addSource(dbSource) { newData ->
            setValue(Resource.loading(newData))
        }
        result().addSource(apiResponse) { response ->
            result().removeSource(apiResponse)
            result().removeSource(dbSource)
            when (response) {
                is SuccessResponse -> {
                    appExecutors.diskIO().execute {
                        val newResponse = processResponse(response)
                        appExecutors.mainThread().execute {
                            completeFetch(response)
                            setValue(Resource.done(newResponse))
                        }
                    }
                }
                is ErrorResponse -> {
                    appExecutors.mainThread().execute {
                        when {
                            response.errorCode == ErrorCodeResponse.UNAUTHORIZED -> {
                                setValue(Resource.unauthorized(response.error))
                            }
                            response.errorCode == ErrorCodeResponse.RATE_LIMIT_EXCEEDED -> {
                                setValue(Resource.limitExceeded(response.error))
                            }
                            response.errorCode == ErrorCodeResponse.NOT_FOUND -> {
                                setValue(Resource.notFoud(response.error))
                            }
                            response.error is IOException -> {
                                setValue(Resource.networkFailed(response.error))
                            }
                            else -> {
                                setValue(Resource.error(response.error))
                            }
                        }
                    }
                }
            }
        }
    }

    fun asLiveData() = result() as LiveData<Resource<Type>>

    private fun setLoadingViewType() {
        when (isLoadingPage) {
            false -> {
                setValue(Resource.loading(null))
            }
            else -> {
                setValue(Resource.loadingPage(null))
            }
        }
    }

    @WorkerThread
    protected open fun processResponse(response: SuccessResponse<Type>) = response.body


    @MainThread
    protected abstract fun completeFetch(successResponse: SuccessResponse<Type>)

    @MainThread
    protected abstract fun result(): MediatorLiveData<Resource<Type>>

    @WorkerThread
    protected abstract fun saveFromNetwork(item: Type)

    @MainThread
    protected abstract fun shouldFetchFromNetwork(data: Type?): Boolean

    @MainThread
    protected abstract fun loadFromLocal(): LiveData<Type>

    @MainThread
    protected abstract fun loadFromNetwork(): LiveData<DataResponse<Type>>

}