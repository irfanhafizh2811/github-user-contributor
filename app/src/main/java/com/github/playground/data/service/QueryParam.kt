package com.github.playground.data.service

object QueryParam {
    const val SORT = "sort"
    const val SORT_ASC = "asc"
    const val SORT_DESC = "desc"

    const val PER_PAGE = "per_page"
    const val PER_PAGE_SIZE = 10
}