package com.github.playground.data.repository

import User
import UserResponse
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.paging.PageKeyedDataSource.LoadInitialCallback
import androidx.paging.PageKeyedDataSource.LoadInitialParams
import androidx.paging.PageKeyedDataSource.LoadCallback
import androidx.paging.PageKeyedDataSource.LoadParams
import com.github.playground.app.AppExecutors
import com.github.playground.data.factory.AbsentLiveData
import com.github.playground.data.response.DataResponse
import com.github.playground.data.response.Resource
import com.github.playground.data.response.SuccessResponse
import com.github.playground.data.service.MainService

class MainRepository(
    private val appExecutors: AppExecutors,
    private val service: MainService
) {

    val resource = MediatorLiveData<Resource<UserResponse>>()
    var incompleteResults = false
    var resourceLiveData: LiveData<Resource<UserResponse>>? = null

    fun searchUsers(
        queryUser: String,
        page: Long,
        initialCallParams: Pair<LoadInitialCallback<Long, User>, LoadInitialParams<Long>>? = null,
        callParams: Pair<LoadCallback<Long, User>, LoadParams<Long>>? = null
    ): LiveData<Resource<UserResponse>> {
        resourceLiveData = object : RepositoryLiveData<UserResponse>(appExecutors, page > 1) {

            override fun result(): MediatorLiveData<Resource<UserResponse>> {
                return resource
            }

            override fun loadFromLocal(): LiveData<UserResponse> {
                return AbsentLiveData.create()
            }

            override fun loadFromNetwork(): LiveData<DataResponse<UserResponse>> {
                return service.searchUsers(queryUser, page)
            }

            override fun completeFetch(successResponse: SuccessResponse<UserResponse>) {
                incompleteResults = successResponse.body.incompleteResults
                initialCallParams?.let {
                    it.first.onResult(
                        successResponse.body.items,
                        null,
                        2
                    )
                }
                callParams?.let {
                    it.first.onResult(
                        successResponse.body.items,
                        it.second.key.inc()
                    )
                }
            }

            override fun saveFromNetwork(item: UserResponse) {}
            override fun shouldFetchFromNetwork(data: UserResponse?): Boolean = true
        }.asLiveData()
        return resourceLiveData as LiveData<Resource<UserResponse>>
    }
}