package com.github.playground.data.factory

import User
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.github.playground.data.paging.MainPageDataSource
import com.github.playground.data.repository.MainRepository
import com.github.playground.utils.TextUtils

class MainDataSourceFactory(var repository: MainRepository) :
    DataSource.Factory<Long, User>() {

    lateinit var dataStore: MainPageDataSource
    val main: MutableLiveData<MainPageDataSource> = MutableLiveData()
    var user: String = TextUtils.BLANK

    override fun create(): DataSource<Long, User> {
        dataStore = MainPageDataSource(repository).apply { queryUser = user }
        return dataStore.also { main.value = it }
    }

}