package com.github.playground.data.response

data class SuccessResponse<T>(val body: T) : DataResponse<T>()