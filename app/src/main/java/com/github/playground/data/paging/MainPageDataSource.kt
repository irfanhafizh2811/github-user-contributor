package com.github.playground.data.paging

import User
import UserResponse
import androidx.lifecycle.LiveData
import androidx.paging.PageKeyedDataSource
import com.github.playground.data.repository.MainRepository
import com.github.playground.data.response.Resource
import com.github.playground.utils.TextUtils

class MainPageDataSource(val repository: MainRepository) :
    PageKeyedDataSource<Long, User>() {

    var queryUser = TextUtils.BLANK

    override fun loadInitial(
        params: LoadInitialParams<Long>,
        callback: LoadInitialCallback<Long, User>
    ) {
        if (queryUser.isNotBlank()) {
            repository.searchUsers(
                queryUser = queryUser,
                page = 1,
                initialCallParams = Pair(callback, params)
            )
            repository.resourceLiveData?.value
        }
    }

    override fun loadAfter(
        params: LoadParams<Long>,
        callback: LoadCallback<Long, User>
    ) {
        if (queryUser.isNotBlank() && !repository.incompleteResults) {
            repository.searchUsers(
                queryUser = queryUser,
                page = params.key,
                callParams = Pair(callback, params)
            )
            repository.resourceLiveData?.value
        }
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, User>) {}

    fun resources(): LiveData<Resource<UserResponse>> {
        return repository.resource
    }
}