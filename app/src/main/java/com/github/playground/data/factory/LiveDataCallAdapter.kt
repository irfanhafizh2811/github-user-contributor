package com.github.playground.data.factory

import androidx.lifecycle.LiveData
import com.github.playground.data.response.DataResponse
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type

class LiveDataCallAdapter<R>(private val responseType: Type) :
    CallAdapter<R, LiveData<DataResponse<R>>> {

    companion object {
        const val UNKNOWN_CODE = -1
    }

    override fun adapt(call: Call<R>): LiveData<DataResponse<R>> {
        return object : LiveData<DataResponse<R>>() {
            private var isSuccess = false

            override fun onActive() {
                super.onActive()
                if (!isSuccess) enqueue()
            }

            override fun onInactive() {
                super.onInactive()
                dequeue()
            }

            private fun dequeue() {
                if (call.isExecuted) call.cancel()
            }

            private fun enqueue() {
                try {
                    call.enqueue(object : Callback<R> {
                        override fun onFailure(call: Call<R>, t: Throwable) {
                            postValue(DataResponse.create(UNKNOWN_CODE, t))
                        }

                        override fun onResponse(call: Call<R>, response: Response<R>) {
                            postValue(DataResponse.create(response))
                            isSuccess = true
                        }
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun responseType(): Type = responseType
}