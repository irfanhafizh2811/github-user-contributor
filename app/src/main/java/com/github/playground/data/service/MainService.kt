package com.github.playground.data.service

import UserResponse
import androidx.lifecycle.LiveData
import com.github.playground.data.response.DataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MainService {

    @GET("search/users")
    fun searchUsers(
        @Query("q") queryUser: String,
        @Query("page") page: Long,
        @Query("per_page") perPage: Int = QueryParam.PER_PAGE_SIZE
    ): LiveData<DataResponse<UserResponse>>

}