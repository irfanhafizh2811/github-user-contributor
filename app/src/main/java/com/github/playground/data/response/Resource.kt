package com.github.playground.data.response

import com.github.playground.view.state.ViewState


data class Resource<out T>(
    var viewState: ViewState,
    val data: T?,
    val throwable: Throwable? = null
) {

    companion object {

        fun <T> empty(data: T? = null): Resource<T> {
            return Resource(ViewState.Empty, data)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(ViewState.Loading, data)
        }

        fun <T> loadingPage(data: T? = null): Resource<T> {
            return Resource(ViewState.LoadingPage, data)
        }

        fun <T> done(
            data: T?
        ): Resource<T> {
            return Resource(ViewState.Done, data)
        }

        fun <T> unauthorized(throwable: Throwable? = null): Resource<T> {
            return Resource(ViewState.Unauthorized, null, throwable)
        }

        fun <T> networkFailed(throwable: Throwable? = null): Resource<T> {
            return Resource(ViewState.NetworkFailed, null, throwable)
        }

        fun <T> limitExceeded(throwable: Throwable? = null): Resource<T> {
            return Resource(ViewState.LimitExceeded, null, throwable)
        }

        fun <T> notFoud(throwable: Throwable? = null): Resource<T> {
            return Resource(ViewState.NotFound, null, throwable)
        }

        fun <T> error(throwable: Throwable? = null): Resource<T> {
            return Resource(ViewState.UnknownError, null, throwable)
        }
    }

}