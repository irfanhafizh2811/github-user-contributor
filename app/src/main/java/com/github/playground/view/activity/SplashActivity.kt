package com.github.playground.view.activity

import android.animation.Animator
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.playground.R
import kotlinx.android.synthetic.main.activity_splash.*
import org.jetbrains.anko.intentFor

class SplashActivity : AppCompatActivity() {

    companion object {
        const val ANIMATION_ASSET_JSON = "github_octocat.json"
        const val ANIMATION_SPEED = 2.5f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        lav_splash?.run {
            setAnimation(ANIMATION_ASSET_JSON)
            speed = ANIMATION_SPEED
            playAnimation()
            addAnimatorListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animator: Animator?) {}

                override fun onAnimationEnd(animator: Animator?) {
                    finish()
                    startActivity(intentFor<MainActivity>())
                }

                override fun onAnimationCancel(animator: Animator?) {}
                override fun onAnimationStart(animator: Animator?) {}
            })
        }
    }

}