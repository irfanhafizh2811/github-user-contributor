package com.github.playground.view.activity

import User
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import com.github.playground.R
import com.github.playground.extension.context.getColorCompat
import com.github.playground.extension.view.visibleIf
import com.github.playground.utils.CustomTabUtils
import com.github.playground.utils.TextUtils
import com.github.playground.view.adapter.MainAdapter
import com.github.playground.view.model.MainViewModel
import com.github.playground.view.state.ViewState
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModel<MainViewModel>()
    private val adapter by lazy { MainAdapter() }
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        disposable.add(
            RxTextView.textChanges(tie_search)
                .skipInitialValue()
                .debounce(DEBOUNCE_TIME_IN_SECOND, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .map { it.toString() }
                .subscribe { query ->
                    rv_main?.visibleIf(query.isNotBlank())
                    viewModel.searchUsers(query)
                }
        )
        setAdapterRecycler()
        viewModel.resources.observe(this, Observer {
            adapter.state = it.viewState
            pb_search?.visibleIf(it.viewState == ViewState.Loading)
            when (it.viewState) {
                ViewState.NetworkFailed -> {
                    showSnackbar(cl_main, getString(R.string.message_no_internet_connection))
                }
                ViewState.LimitExceeded -> {
                    showSnackbar(cl_main, it.throwable?.message ?: TextUtils.BLANK)
                }
            }
        })
        viewModel.paged.observe(this, Observer {
            adapter.submitList(it)
        })
    }

    private fun setAdapterRecycler() {
        adapter.setOnUserProfileListener { user ->
            openBrowserTab(user)
        }
        rv_main?.adapter = this.adapter
    }

    private fun openBrowserTab(user: User) {
        val tabsIntent = CustomTabsIntent.Builder().apply {
            setToolbarColor(getColorCompat(R.color.colorPrimary))
            addDefaultShareMenuItem()
            setShowTitle(true)
            setExitAnimations(
                this@MainActivity,
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }.build()
        val packageName = CustomTabUtils.getPackageNameToUse(this, user.htmlUrl)
        packageName?.let {
            tabsIntent.intent.setPackage(it)
            tabsIntent.launchUrl(this, Uri.parse(user.htmlUrl))
        }
    }

    private fun showSnackbar(coordinatorLayout: CoordinatorLayout, message: String) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG)
            .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

    companion object {
        const val DEBOUNCE_TIME_IN_SECOND = 1L
    }

}