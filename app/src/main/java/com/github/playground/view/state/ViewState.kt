package com.github.playground.view.state

sealed class ViewState {
    object Empty : ViewState()
    object Loading : ViewState()
    object LoadingPage : ViewState()
    object Done : ViewState()
    object NotFound : ViewState()
    object Unauthorized : ViewState()
    object LimitExceeded : ViewState()
    object NetworkFailed : ViewState()
    object UnknownError : ViewState()
}
