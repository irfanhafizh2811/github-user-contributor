package com.github.playground.view.adapter

import User
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.playground.R
import com.github.playground.extension.glide.loadFromUrl
import com.github.playground.extension.view.visibleIf
import com.github.playground.view.state.ViewState
import kotlinx.android.synthetic.main.layout_item_loading.view.*
import kotlinx.android.synthetic.main.layout_item_user.view.*

class MainAdapter(diffCallback: ItemCallback<User> = MainItemCallback) :
    PagedListAdapter<User, RecyclerView.ViewHolder>(diffCallback) {

    lateinit var state: ViewState
    private var onUserProfileListener: ((User) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            ViewType.User.ordinal -> {
                MainViewHolder(
                    LayoutInflater
                        .from(parent.context)
                        .inflate(
                            R.layout.layout_item_user,
                            parent,
                            false
                        )
                )
            }
            ViewType.Loading.ordinal -> {
                LoadingViewHolder(
                    LayoutInflater
                        .from(parent.context)
                        .inflate(
                            R.layout.layout_item_loading,
                            parent,
                            false
                        )
                )
            }
            else -> throw RuntimeException("Illegal view type")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MainViewHolder -> getItem(holder.adapterPosition)?.let { holder.bind(it) }
            is LoadingViewHolder -> holder.bind(state)
        }
    }

    override fun getItemViewType(position: Int): Int = when (position) {
        itemCount.minus(1) -> ViewType.Loading.ordinal
        else -> ViewType.User.ordinal
    }

    fun setOnUserProfileListener(onUserProfileListener: (User) -> Unit) {
        this.onUserProfileListener = onUserProfileListener
    }

    inner class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(user: User) {
            with(itemView) {
                iv_avatar?.loadFromUrl(user.avatarUrl)
                tv_user?.text = user.login
                btn_profile?.setOnClickListener { onUserProfileListener?.invoke(user) }
            }
        }
    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(state: ViewState) {
            with(itemView) {
                pb_cycle?.visibleIf(state is ViewState.LoadingPage)
            }
        }
    }

    object MainItemCallback : DiffUtil.ItemCallback<User>() {

        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem == newItem
    }

    enum class ViewType {
        User,
        Loading
    }

}