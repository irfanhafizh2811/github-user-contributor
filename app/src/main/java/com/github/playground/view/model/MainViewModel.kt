package com.github.playground.view.model

import User
import UserResponse
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.github.playground.app.AppExecutors
import com.github.playground.data.factory.MainDataSourceFactory
import com.github.playground.data.paging.MainPageDataSource
import com.github.playground.data.response.Resource
import com.github.playground.data.service.QueryParam
import com.github.playground.extension.clazz

class MainViewModel(
    private val dataSourceFactory: MainDataSourceFactory,
    private val appExecutors: AppExecutors
) : ViewModel(), LifecycleOwner {

    override fun getLifecycle(): Lifecycle = LifecycleRegistry(this)

    private val config = PagedList.Config.Builder().apply {
        setEnablePlaceholders(true)
        setPageSize(QueryParam.PER_PAGE_SIZE)
    }.build()

    val resources: LiveData<Resource<UserResponse>> = Transformations
        .switchMap<MainPageDataSource, Resource<UserResponse>>(
            dataSourceFactory.main,
            MainPageDataSource::resources
        )

    val paged: LiveData<PagedList<User>>
        get() = LivePagedListBuilder<Long, User>(
            dataSourceFactory,
            config
        ).apply {
            setFetchExecutor(appExecutors.mainThread())
        }.build()

    fun searchUsers(query: String) {
        dataSourceFactory.apply {
            user = query
            dataStore.invalidate()
        }
    }

}