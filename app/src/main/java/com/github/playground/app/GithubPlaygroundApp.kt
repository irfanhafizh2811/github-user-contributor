package com.github.playground.app

import androidx.multidex.MultiDexApplication
import com.github.playground.di.librariesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class GithubPlaygroundApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(librariesModule)
            androidContext(this@GithubPlaygroundApp)
        }
    }
}