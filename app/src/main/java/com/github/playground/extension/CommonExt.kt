package com.github.playground.extension

import android.content.Context
import android.util.TypedValue
import com.github.playground.utils.TextUtils
import java.text.DecimalFormat
import java.text.NumberFormat
import kotlin.math.roundToInt

inline fun <reified T : Any> clazz() = T::class.java

inline fun <reified T : Any> tag() = T::class.java.simpleName

fun Long.convertToCurrency(currencyType: String = TextUtils.BLANK): String {
    val value = NumberFormat.getNumberInstance().format(this).replace(',', '.')
    return currencyType + value
}

fun Double.doubleToString(decimalPattern: String = "#.#"): String {
    return try {
        val decimalFormat = DecimalFormat(decimalPattern)
        decimalFormat.format(this)
    } catch (e: Exception) {
        this.toString()
    }
}

fun Float.convertDpToPx(context: Context): Int {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, this, context.resources.displayMetrics
    ).roundToInt()
}

fun Context.dpToPx(dp: Int): Int {
    val density = resources.displayMetrics.density
    return (dp * density).roundToInt()
}
