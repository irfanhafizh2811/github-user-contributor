## Getting Start

You can open this Project with Android Studio *3.4.1*.

## Prerequisites

What things you need to install the software and install them :
If you using Mac OS, you could install this with [Homebrew](homebrew.sh)

- Java 8
- Gradle

## Installing

A step by step to get a development env running

```
./gradlew assembleDevDebug
```

To install in emulator or device
```
./gradlew installDevDebug
```

To check dependencies using

```
./gradlew app:dependencies
```

| **Splash Screen** | **Main Screen** | **Browser tab  Screen** |
| ------ | ------ | ------ |
| ![alt text](https://i.ibb.co/tBVB9L7/Screen-Shot-2020-03-30-at-11-01-05.png "Splash Screen") | ![alt text](https://i.ibb.co/HGVJ7fs/Screen-Shot-2020-03-30-at-11-01-12.png "Main Screen") | ![alt text](https://i.ibb.co/6ZLws8s/Screen-Shot-2020-03-30-at-11-01-19.png "Browser tab Screen") |


## Lottiefiles 
![alt text](https://i.ibb.co/DYgJvSF/Screen-Shot-2020-03-30-at-11-25-46.png "Lottiefiles")

Lottiefiles source github animation
```
https://lottiefiles.com/9867-github-logo-octocat-animated
```

**Artist** Ali Torabian 
```
https://lottiefiles.com/user/64607
```